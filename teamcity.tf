provider "docker" {
  host = "tcp://127.0.0.1:2375/"
}

resource "docker_container" "teamcity-server" {
  name  = "teamcity-server-instance"
  image = "${docker_image.teamcity-server.latest}"

  volumes {
    host_path      = "c:\\users\\Julian\\projects\\teamcity\\data"
    container_path = "/data/teamcity_server/datadir"
  }

  volumes {
    host_path      = "c:\\users\\Julian\\projects\\teamcity\\logs"
    container_path = "/opt/teamcity/logs"
  }

  ports {
    internal = "8111"
    external = "8111"
  }

  network_mode = "bridge"

  networks_advanced {
    name    = "${docker_network.private_network.name}"
    aliases = ["teamcity_host"]
  }
}

resource "docker_container" "teamcity-postgres" {
  name  = "teamcity-postgres"
  image = "${docker_image.teamcity-postgres.latest}"

  volumes {
    volume_name    = "postgres-volume"
    container_path = "/var/lib/postgresql/data"
  }

  ports {
    internal = "5432"
    external = "5432"
  }

  network_mode = "bridge"

  networks_advanced {
    name    = "${docker_network.private_network.name}"
    aliases = ["postgres_host"]
  }

  env      = ["POSTGRES_USER=teamcity", "POSTGRES_PASSWORD=jetbrains", "POSTGRES_DB=teamcity"]
  must_run = "true"
}

resource "docker_container" "teamcity-agent-npm-a" {
  name  = "teamcity-agent-npm-a"
  image = "${docker_image.teamcity-agent-npm.latest}"

  volumes {
    host_path      = "c:\\users\\Julian\\projects\\teamcity\\config-a"
    container_path = "/data/teamcity_agent/conf"
  }

  volumes {
    host_path      = "/var/run/docker.sock"
    container_path = "/var/run/docker.sock"
  }

  volumes {
    host_path      = "/opt/buildagent/work"
    container_path = "/opt/buildagent/work"
  }

  volumes {
    host_path      = "/opt/buildagent/temp"
    container_path = "/opt/buildagent/temp"
  }

  volumes {
    host_path      = "/opt/buildagent/tools"
    container_path = "/opt/buildagent/tools"
  }

  volumes {
    host_path      = "/opt/buildagent/plugins"
    container_path = "/opt/buildagent/plugins"
  }

  volumes {
    host_path      = "/opt/buildagent/system"
    container_path = "/opt/buildagent/system"
  }

  networks_advanced {
    name = "${docker_network.private_network.name}"
  }

  env = ["SERVER_URL=teamcity-server-instance:8111", "AGENT_NAME=agent-npm-a"]
}

resource "docker_container" "teamcity-agent-npm-b" {
  name  = "teamcity-agent-npm-b"
  image = "${docker_image.teamcity-agent-npm.latest}"

  volumes {
    host_path      = "c:\\users\\Julian\\projects\\teamcity\\config-b"
    container_path = "/data/teamcity_agent/conf"
  }

  volumes {
    host_path      = "/var/run/docker.sock"
    container_path = "/var/run/docker.sock"
  }

  volumes {
    host_path      = "/opt/buildagent/work"
    container_path = "/opt/buildagent/work"
  }

  volumes {
    host_path      = "/opt/buildagent/temp"
    container_path = "/opt/buildagent/temp"
  }

  volumes {
    host_path      = "/opt/buildagent/tools"
    container_path = "/opt/buildagent/tools"
  }

  volumes {
    host_path      = "/opt/buildagent/plugins"
    container_path = "/opt/buildagent/plugins"
  }

  volumes {
    host_path      = "/opt/buildagent/system"
    container_path = "/opt/buildagent/system"
  }

  networks_advanced {
    name = "${docker_network.private_network.name}"
  }

  env = ["SERVER_URL=teamcity-server-instance:8111", "AGENT_NAME=agent-npm-b"]
}

resource "docker_container" "teamcity-agent-c" {
  name  = "teamcity-agent-c"
  image = "${docker_image.teamcity-agent.latest}"

  volumes {
    volume_name    = "agent-volume"
    container_path = "/data/teamcity_agent/conf"
  }

  volumes {
    host_path      = "/var/run/docker.sock"
    container_path = "/var/run/docker.sock"
  }

  volumes {
    host_path      = "/opt/buildagent/work"
    container_path = "/opt/buildagent/work"
  }

  volumes {
    host_path      = "/opt/buildagent/temp"
    container_path = "/opt/buildagent/temp"
  }

  volumes {
    host_path      = "/opt/buildagent/tools"
    container_path = "/opt/buildagent/tools"
  }

  volumes {
    host_path      = "/opt/buildagent/plugins"
    container_path = "/opt/buildagent/plugins"
  }

  volumes {
    host_path      = "/opt/buildagent/system"
    container_path = "/opt/buildagent/system"
  }

  networks_advanced {
    name = "${docker_network.private_network.name}"
  }

  env = ["SERVER_URL=teamcity-server-instance:8111", "AGENT_NAME=agent-c"]
}

resource "docker_image" "teamcity-server" {
  name = "jetbrains/teamcity-server"
}

resource "docker_image" "teamcity-postgres" {
  name = "postgres"
}

resource "docker_image" "teamcity-agent-npm" {
  name = "julianbrown/teamcity-agent-npm"
}

resource "docker_image" "teamcity-agent" {
  name = "jetbrains/teamcity-agent"
}

resource "docker_network" "private_network" {
  name = "teamcity-network"
}

resource "docker_volume" "shared_volume" {
  name = "postgres-volume"
}

resource "docker_volume" "shared_agent_volume" {
  name = "agent-volume"
}
