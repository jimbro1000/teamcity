docker run -d -it --name teamcity-postgres -v ./postgres:/var/lib/postgresql/data -e POSTGRES_USER=teamcity -e POSTGRES_PASSWORD=jetbrains -e POSTGRES_DB=teamcity -p 5432:5432 postgres

docker run -d -it --name teamcity-server-instance -v ./data:/data/teamcity_server/datadir -v ./logs:/opt/teamcity/logs -p 8111:8111 --link teamcity-postgres:postgres jetbrains/teamcity-server

docker run -d -it --name teamcity-agent-npm-a -e SERVER_URL="10.0.75.1:8111" -e AGENT_NAME="agent-npm-a" -v ./config-b:/data/teamcity_agent/conf -v /var/run/docker.sock:/var/run/docker.sock -v /opt/buildagent/work:/opt/buildagent/work -v /opt/buildagent/temp:/opt/buildagent/temp -v /opt/buildagent/tools:/opt/buildagent/tools -v /opt/buildagent/plugins:/opt/buildagent/plugins -v /opt/buildagent/system:/opt/buildagent/system  julianbrown/teamcity-agent-npm:r1

docker run -d -it --name teamcity-agent-npm-b -e SERVER_URL="10.0.75.1:8111" -e AGENT_NAME="agent-npm-b" -v ./config-a:/data/teamcity_agent/conf -v /var/run/docker.sock:/var/run/docker.sock -v /opt/buildagent/work:/opt/buildagent/work -v /opt/buildagent/temp:/opt/buildagent/temp -v /opt/buildagent/tools:/opt/buildagent/tools -v /opt/buildagent/plugins:/opt/buildagent/plugins -v /opt/buildagent/system:/opt/buildagent/system julianbrown/teamcity-agent-npm:r1
